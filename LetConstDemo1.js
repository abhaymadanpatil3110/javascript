
let a = 10;
console.log(a);
const b = null;//no error even for undefined also
console.log(b);

a = 40;
//b = 30; this line gives error of type error bc const cant be assigned

console.log(a);
console.log(b);
