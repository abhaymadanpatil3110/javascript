
function x() {

	var a = 10;

	function y() {
	
		document.write(10);	
		console.log(a);
		return x;	// it return undefined bc it is like variable 
	}
	return y;	//it will return function y with it lexical scope and thats the difference when you return different functions and from different scenarios,  the reason is that y has lexical parent x but how it can return bc if it return that x then how will lexical chaining will work and same applied to vice versa for returning y funciton
}
var b = x();
console.log(b);
b();
