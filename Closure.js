
function x() {

	var a = 10;
	
	function y() {
	
		console.log(a);
	}
	return y;	// it is called as closure the reason is, return statement returns function y with its lexical scope in bundled manner and that get stored in other variable then that variable is also becomes function and whenever and at anywhere when you call that function then who stored return value of it , it will rememeber that bundled information and will not give error that is special in js which other languages dont have. 
}
