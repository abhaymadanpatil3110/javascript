
let a = 10;
const b = 20;
var c = 30;

	console.log("a: "+a+"script");
	console.log("b: "+b+"script");
	console.log("c: "+c+"global space");
{

	let a = 40;
	const b = 50;
	var c = 60;

	console.log("a: "+a+"block1")
	console.log("b "+b+"block1")
	console.log("c "+c+"block1")
	{
	
		let a = 70;
		const b = 80;
		var c = 90;
		console.log("a: "+a+"block2")
		console.log("b: "+b+"block2")
		console.log("c: "+c+"block2")
	}
}
	console.log("a: "+a+" :if a = 10 then it is script")
	console.log("b: "+b+": if b = 20 then it is script")
	console.log("c: "+c+": if c = 30 then it is wrong bc in block2 it is already overriden")
